#ifndef __NEWTON_MATH__TEST_MATRIX_HPP__ 
#define __NEWTON_MATH__TEST_MATRIX_HPP__ 

#include <cstddef>
#include <cstdint>
#include <Math/matrix.hpp>


void test_matrix_col_major () {
	using namespace Newton::Math;
	using namespace Newton::Math::col_major;
	// Test determinant 	
	([] () {
		auto m = tmat_make (tvec_make (1, 3), tvec_make (2, 4));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == -2);
		assert (det (n) == -2);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (5, 1), tvec_make (4, 1));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 1);
		assert (det (n) == 1);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (5, -1), tvec_make (3, 4));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 23);
		assert (det (n) == 23);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (1, 0), tvec_make (-4, 3));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 3);
		assert (det (n) == 3);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (2, -1), tvec_make (1, 3));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 7);
		assert (det (n) == 7);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (1, 0, 0), tvec_make (2, -4, 3), tvec_make (3, 1, -1));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 1);
		assert (det (n) == 1);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (5, 0, 2), tvec_make (-2, 3, 0), tvec_make (1, -1, 7));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 103);
		assert (det (n) == 103);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (2, -6, -2), tvec_make (-3, 3, -3), tvec_make (-2, 3, -2));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 12);
		assert (det (n) == 12);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (-4, -3, -1), tvec_make (5, 4, 2), tvec_make (2, 2, 5));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == -3);
		assert (det (n) == -3);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (1, 0, -1, 2), tvec_make (4, 1, 0, 0), tvec_make (2, 4, 1, 4), tvec_make (3, 4, 0, 1));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 65);
		assert (det (n) == 65);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (m));
		assert (on*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (3, 2, 0, -1), tvec_make (2, 1, 5, 2), tvec_make (-1, 5, 2, 1), tvec_make (4, 7, -6, 0));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == -418);
		assert (det (n) == -418);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (m));
		assert (on*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (3, 1, 4, 5), tvec_make (0, 2, 0, 0), tvec_make (2, 0, 6, 2), tvec_make (-1, -2, -3, 0));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 20);
		assert (det (n) == 20);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (m));
		assert (on*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (n));
		
		std::cout << "---^\n";

	}) ();
	
}
void test_matrix_row_major () {
	using namespace Newton::Math;
	using namespace Newton::Math::row_major;
	// Test determinant 	
	([] () {
		auto m = tmat_make (tvec_make (1, 2), tvec_make (3, 4));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == -2);
		assert (det (n) == -2);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (5, 4), tvec_make (1, 1));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 1);
		assert (det (n) == 1);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (5, 3), tvec_make (-1, 4));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 23);
		assert (det (n) == 23);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (1, -4), tvec_make (0, 3));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 3);
		assert (det (n) == 3);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (2, 1), tvec_make (-1, 3));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 7);
		assert (det (n) == 7);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2) == tvec_make (1, 2)*det (m));
		assert (on*tvec_make (1, 2) == tvec_make (1, 2)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (1, 2, 3), tvec_make (0, -4, 1), tvec_make (0, 3, -1));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 1);
		assert (det (n) == 1);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (5, -2, 1), tvec_make (0, 3, -1), tvec_make (2, 0, 7));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 103);
		assert (det (n) == 103);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (2, -3, -2), tvec_make (-6, 3, 3), tvec_make (-2, -3, -2));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 12);
		assert (det (n) == 12);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (-4, 5, 2), tvec_make (-3, 4, 2), tvec_make (-1, 2, 5));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == -3);
		assert (det (n) == -3);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (m));
		assert (on*tvec_make (1, 2, 3) == tvec_make (1, 2, 3)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (1, 4, 2, 3), tvec_make (0, 1, 4, 4), tvec_make (-1, 0, 1, 0), tvec_make (2, 0, 4, 1));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 65);
		assert (det (n) == 65);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (m));
		assert (on*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (3, 2, -1, 4), tvec_make (2, 1, 5, 7), tvec_make (0, 5, 2, -6), tvec_make (-1, 2, 1, 0));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == -418);
		assert (det (n) == -418);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (m));
		assert (on*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (n));
		
		std::cout << "---^\n";

	}) ();
	([] () {
		auto m = tmat_make (tvec_make (3, 0, 2, -1), tvec_make (1, 2, 0, -2), tvec_make (4, 0, 6, -3), tvec_make (5, 0, 2, 0));
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == 20);
		assert (det (n) == 20);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (m));
		assert (on*tvec_make (1, 2, 3, 4) == tvec_make (1, 2, 3, 4)*det (n));
		
		std::cout << "---^\n";

	}) ();
	
}
#endif // __NEWTON_MATH__TEST_MATRIX_HPP__