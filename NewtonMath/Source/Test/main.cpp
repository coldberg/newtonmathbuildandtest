#define _NEWTON_MATH_RANGE_CHECK 1
#include <Math/vector.hpp>
#include <Math/matrix.hpp>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <chrono>
#include <fstream>
#include <vector>
#include <memory>

#include "matrix.hpp"


using namespace Newton::Math;
using namespace Newton::Math::row_major;

volatile void _touch (volatile void* touch) {
    volatile void* t = touch;
}

int main (int, char**) {

    test_matrix_row_major ();
    test_matrix_col_major ();

/*
    std::cout << "---\n";
    std::cout << "Performance test , caclulating batch of mat4 determinants...\n";
    std::cout << "Preparing data...\n";    

    const std::size_t size = 1024*1024;
    auto data = std::make_unique<mat4 []>(size);
    auto odet = std::make_unique<float []>(size);     
    std::ifstream cache_load ("test_data.cache", std::ios::binary);

    if (!cache_load) {
        for (auto i = 0u; i < size; ++i) {
            data.get() [i] = mat4 (
                (float)rand (), (float)rand (), (float)rand (), (float)rand (),
                (float)rand (), (float)rand (), (float)rand (), (float)rand (),
                (float)rand (), (float)rand (), (float)rand (), (float)rand (),
                (float)rand (), (float)rand (), (float)rand (), (float)rand ()
            );
        }
        std::ofstream cache ("test_data.cache", std::ios::binary);
        cache.write ((const char*)data.get (), size*sizeof (mat4));
    }
    else {
        cache_load.read ((char*)data.get (), size*sizeof (mat4));
    }
    
    auto tot = 0.0;
    auto cnt = 0.0;
    for (auto k = 0; k < 8; ++k) {
        std::cout << "Running test...\n";
        auto t0 = std::chrono::high_resolution_clock::now ();
        for (auto j = 0; j < 128; ++j) {
            for (auto i = 0; i < size; ++i) {
                odet.get () [i] = det (data.get () [i]);
            }
        }
        auto t1 = std::chrono::high_resolution_clock::now ();
        auto dt = (std::chrono::duration_cast<std::chrono::microseconds> (t1 - t0).count ()*1e-6);
        std::cout << "Done, time elapsed: " << dt << "\n";
        tot += dt;
        ++cnt;
    }
    std::cout << "Avg. time: " << (tot/cnt) << "\n";
    std::ofstream out ("NUL", std::ios::binary);
    out.write ((const char*)odet.get (), size*sizeof (float));
*/    
    return 0;
}
