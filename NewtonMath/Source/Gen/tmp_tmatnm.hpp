<?php
	$G_cols = $G_matrix_size [0];
	$G_rows = $G_matrix_size [1];
	$G_major_size = $G_row_major ? $G_rows : $G_cols;
	$G_minor_size = $G_row_major ? $G_cols : $G_rows;
	$G_major_type = $G_row_major ? 'row_type' : 'col_type';
	$G_minor_type = $G_row_major ? 'col_type' : 'row_type';
	$G_major_vget = $G_row_major ? 'row' : 'col';
	$G_minor_vget = $G_row_major ? 'col' : 'row';
	$G_min_size = min ($G_cols, $G_rows);
	$G_max_size = max ($G_cols, $G_rows);
	require_once 'tmp_tvec_functions.php';
?>

#include "basic_tmat.hpp"
#include "tvec<?php echo $G_rows ; ?>.hpp"
<?php if ($G_cols != $G_rows): ?>
#include "tvec<?php echo $G_cols ; ?>.hpp"
<?php endif; ?>
#pragma pack(push, 1)
namespace <?php echo $G_namespace ; ?> {
	namespace <?php echo $G_module_name ; ?> {
		namespace <?php echo ($G_row_major ? 'row' : 'col') ; ?>_major {			
			template <typename T>
			struct tmat<<?php echo implode (', ', [$G_cols, $G_rows]); ?>, T> {
				static const std::size_t cols = <?php echo $G_cols ; ?>u;
				static const std::size_t rows = <?php echo $G_rows ; ?>u;
				typedef T value_type;
				typedef tvec<<?php echo $G_cols ; ?>, T> row_type;
				typedef tvec<<?php echo $G_rows ; ?>, T> col_type;
				
				explicit constexpr tmat (<?php
					$out = [];
					$set = [];				
					for ($i = 0; $i < $G_major_size; ++$i) {
						$set [] = sprintf ('v%d', $i);
						$out [] = sprintf ('const %s& %s', $G_row_major ? 'row_type' : 'col_type', end ($set));
					}
					echo implode (', ', $out);
				?>) : m_data {<?php 
					echo implode (', ', $set);
				?>} {}
				
				explicit constexpr tmat (<?php 
					$out = []; $set = [];
					
					for ($j = 0; $j < $G_major_size; ++$j) {
						$line = [];
						for ($i = 0; $i < $G_minor_size; ++$i) {
							$out [] = sprintf ('value_type v%d%d', $i, $j);
							$line [] = sprintf ('v%d%d', $i, $j);							
						}
						$set [] = sprintf ('%s {%s}', $G_major_type, implode (', ', $line));
					}
					
					echo implode (', ', $out);
				?>) : m_data {<?php 
					echo implode (', ', $set);
				?>} {}			
				
				explicit constexpr tmat (value_type w) : 
					m_data {<?php 
						
						$vect = sprintf ('tvec<%d, T>', $G_min_size);
						$init = [];
						for ($j = 0; $j < $G_min_size; ++$j) {
							$line = [];
							for ($i = 0; $i < $G_min_size; ++$i) {
								$line [] = $j != $i ? 'value_type (0)' : 'value_type (w)' ;
							}
							$init [] = sprintf ('%s (%s (%s))', $G_major_type, $vect, implode (', ', $line));
						}
						echo implode (', ', $init);
					?>}
				{}
				
				constexpr tmat () : tmat (value_type (0)) {}
<?php foreach ($G_vector_sizes as $size): ?>			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<<?php echo sprintf ($G_row_major ? 'K, %d' : '%d, K', $size) ; ?>, I>& in) :
					tmat (<?php 
						$out = [];					
						for ($i = 0; $i < $G_major_size; ++$i) {						
							$out [] =  $i < $size ? sprintf ('%s (in [%d])', $G_major_type, $i): sprintf ('%s (value_type (0))', $G_major_type);
						}
						echo implode (', ', $out);
					?>) {}
<?php endforeach; ?>					
				
				const auto& operator [] (std::size_t i) const {
					#if <?php echo make_macro_name ('RANGE_CHECK') ; ?>					
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator [] (std::size_t i) {
					#if <?php echo make_macro_name ('RANGE_CHECK') ; ?> 				
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
<?php foreach (['', 'const '] as $type): ?>
				<?php echo $type ?>auto& operator () (std::size_t col, std::size_t row) <?php echo $type ; ?>{
<?php if ($G_row_major): ?>
					return (*this) [row][col];
<?php else: ?>
					return (*this) [col][row];
<?php endif; ?>	
				}
<?php endforeach; ?>
				
			private:
				<?php echo $G_major_type ; ?> m_data [<?php echo $G_major_size ; ?>];
			};

			template <typename T>
			inline const auto& <?php echo $G_row_major ? 'row' : 'col' ; ?> (const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, T>& in, std::size_t i) {
				return in [i];
			}
			
			template <typename T>
			inline auto <?php echo $G_row_major ? 'col' : 'row' ; ?> (const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, T>& in, std::size_t i) {
				return tvec<<?php echo $G_major_size; ?>, T> (<?php 
					$out = [];
					for ($i = 0; $i < $G_major_size; ++$i) {
						$out [] = sprintf ('in [%d][i]', $i);
					}
					echo implode (', ', $out);
				?>);
			}
			
			template <typename T>
			inline auto transpose (const tmat<<?php echo $G_cols ; ?>, <?php echo $G_rows ; ?>, T>& in) {
				return tmat_make (<?php					
					$out = [];
					for ($i = 0; $i < $G_minor_size; ++$i) {
						$out [] = sprintf ('%s (in, %d)', $G_minor_vget, $i);
					}
					echo implode (', ', $out);
				?>);
			} 
<?php foreach (['*', '/', '+', '-', '%', '&', '|', '^', '||', '&&'] as $op): ?>
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator <?php echo $op ; ?> (const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (<?php 
					$out = [];
					for ($i = 0; $i < $G_major_size; ++$i) {
						$out [] = sprintf ('lhs [%d] %s rhs', $i, $op);
					}
					echo implode (', ', $out);
				?>);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator <?php echo $op ; ?> (const _Lhs& lhs, const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, _Rhs>& rhs) {
				return tmat_make (<?php 
					$out = [];
					for ($i = 0; $i < $G_major_size; ++$i) {
						$out [] = sprintf ('lhs %s rhs [%d]', $op, $i);
					}
					echo implode (', ', $out);
				?>);
			}
			
<?php endforeach; ?>
<?php foreach (['+', '-'] as $op): ?>
			template <typename _Lhs, typename _Rhs>
			inline auto operator <?php echo $op ; ?> (const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, _Lhs>& lhs, const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, _Rhs>& rhs) {
				return tmat_make (<?php 
					$out = [];
					for ($i = 0; $i < $G_major_size; ++$i) {
						$out [] = sprintf ('lhs [%d] %s rhs [%d]', $i, $op, $i);
					}
					echo implode (', ', $out);
				?>);				
			}

<?php endforeach; ?>			
<?php foreach (['==' => '&&', '!=' => '||'] as $op => $join): ?>
			template <typename _Lhs, typename _Rhs>
			inline auto operator <?php echo $op ; ?> (const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, _Lhs>& lhs, const tmat<<?php echo $G_cols; ?>, <?php echo $G_rows ; ?>, _Rhs>& rhs) {
				return <?php 
					$out = [];
					for ($i = 0; $i < $G_major_size; ++$i) {
						$out [] = sprintf ('lhs [%d] %s rhs [%d]', $i, $op, $i);
					}
					echo implode (" ".$join." ", $out);
				?>;				
			}

<?php endforeach; ?>
			template <typename T> 
			inline std::ostream& operator << (std::ostream& oss, const tmat<<?php echo implode (', ', [$G_cols, $G_rows]); ?>, T>& in) {
<?php for ($j = 0; $j < $G_rows; ++$j): ?>
<?php for ($i = 0; $i < $G_cols; ++$i): ?>
				oss << std::setw(5) << in (<?php echo implode (', ', [$i, $j]); ?>) << ", ";
<?php endfor; ?>
				oss << std::endl ;
<?php endfor; ?>				return oss;
			}
			
			template <typename T> 
			inline std::istream& operator >> (std::istream& iss, tmat<<?php echo implode (', ', [$G_cols, $G_rows]); ?>, T>& in) {
<?php for ($j = 0; $j < $G_rows; ++$j): ?>
<?php for ($i = 0; $i < $G_cols; ++$i): ?>
				iss >> in (<?php echo implode (', ', [$i, $j]); ?>);
<?php endfor; ?>
<?php endfor; ?>				return iss;
			}	
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<<?php echo implode (', ', [$G_cols, $G_rows]); ?>, _Lhs>& lhs, const tmat<<?php echo implode (', ', [$G_rows, $G_cols]); ?>, _Rhs>& rhs) {
				tmat<<?php echo implode (', ', [$G_rows, $G_rows]); ?>, _Ret> result;				
<?php 
	for ($j = 0; $j < $G_rows; ++$j): 		
	for ($i = 0; $i < $G_cols; ++$i): 
?>				result (<?php echo implode (', ', [$i, $j]); ?>) = dot (row (lhs, <?php echo $j ; ?>), col (rhs, <?php echo $i ; ?>));					
<?php 
	endfor;
	endfor; 
?>
				return result;
			}
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<<?php echo implode (', ', [$G_cols, $G_rows]); ?>, _Lhs>& lhs, const tvec<<?php echo $G_cols; ?>, _Rhs>& rhs) {
				tvec<<?php echo $G_rows ; ?>, _Ret> result;
<?php for ($j = 0; $j < $G_rows; ++$j): ?>				result [<?php echo $j ; ?>] = dot (row (lhs, <?php echo $j ; ?>), rhs);
<?php endfor; ?>				return result;
			}

			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tvec<<?php echo $G_rows; ?>, _Lhs>& lhs, const tmat<<?php echo implode (', ', [$G_cols, $G_rows]); ?>, _Rhs>& rhs) {
				tvec<<?php echo $G_cols ; ?>, _Ret> result;
<?php for ($j = 0; $j < $G_cols; ++$j): ?>				result [<?php echo $j ; ?>] = dot (lhs, col (rhs, <?php echo $j ; ?>));
<?php endfor; ?>				return result;
			}			
			
			

		}
	}
}
#pragma pack(pop)


 