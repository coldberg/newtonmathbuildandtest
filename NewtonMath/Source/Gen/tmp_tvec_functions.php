<?php 
	function make_macro_name ($name) {
		global $G_module_name, $G_namespace;
		return sprintf ('_%s_%s_%s', strtoupper ($G_namespace), strtoupper ($G_module_name), $name);
	}

	function get_tvec_value ($i, $name) {
		global $G_member_naming;
		$naming = $G_member_naming [0];
		if ($i < count ($naming))
			return sprintf ('%s.%s', $name, $naming [$i]);
		return sprintf ('%s [%i]', $name, $i);
	}
	
	function make_tvec_name ($i, $prefix = 'v', $suffix = '') {
		global $G_member_naming;
		$naming = $G_member_naming [0];
		if ($i < count ($naming))
			return $naming [$i];
		return $prefix.$i.$suffix;
	}
	
	function unwrap_argument_list ($inArgs, $default, $size, $G_vector_max_size) {
		$outArgs = [];
		for ($i = 0; $i < $size; ++$i) {
			$outArgs [] = $i < $inArgs ? get_tvec_value($i, 'in') : $default;			
		}
		echo implode (', ', $outArgs);
	}

	function _build_tvec_initializers ($accum, $inArgs, $outArgs, $size, &$out) {
		global $G_vector_max_size, $G_size;
		
		if ($accum > $size || count ($inArgs) == $size) {
			return;
		}
		$type_name = 'tvec';
		if ($accum == $size) {		
			$out [] = sprintf (
				"%s%s (%s) : %s (%s) {}\n",
				count ($inArgs) > 1 ? "explicit " : "",
				$type_name,
				implode (', ', $inArgs),
				$type_name,
				implode (', ', $outArgs)
			);
			return;
		}
					
		for ($i = 1; $i <= $G_vector_max_size; ++$i) {
			$newInArgs = $inArgs;
			$newOutArgs = $outArgs;
			$name = sprintf ('v%d', count($inArgs));
			if ($i < 2) {						
				$newInArgs [] = 'T '.$name;
				$newOutArgs [] = $name;
			}
			else {
				$newInArgs [] = sprintf ('const %s<%d, T>& %s', $type_name, $i, $name);
				for ($j = 0; $j < $i; ++$j) {
					$newOutArgs [] = get_tvec_value ($j, $name);
				}
			}
			_build_tvec_initializers ($accum + $i, $newInArgs, $newOutArgs, $size, $out);
		}
	}
	
	function build_tvec_initializers ($size) {		
		$out = [];
		_build_tvec_initializers (0, [], [], $size, $out);		
		return $out;
	}
	
	function _build_swizzle_permutations ($string, $indexes, $size, $components, &$out) {
		if ($size <= 0) {			
			$out [] = [$string, $indexes];
			return;
		}		
		for ($i = 0; $i < count ($components); ++$i) {
			_build_swizzle_permutations ($string . $components[$i], array_merge ($indexes, [$i]), $size - 1, $components, $out);
		}
	}
	
	function build_swizzle_permutations ($sizes, $components) {
		$out = [];
		foreach ($sizes as $size) {
			_build_swizzle_permutations ('', [], $size, $components, $out);
		}
		return $out;
	}
	