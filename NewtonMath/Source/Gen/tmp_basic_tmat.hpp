// basic_tmat
#include <cstddef>
#include <cstdint>
#include <iomanip>
#include <iostream>


namespace <?php echo $G_namespace ; ?> {
	namespace <?php echo $G_module_name ; ?> {		
<?php foreach (['col_major', 'row_major'] as $type): ?>				
		namespace <?php echo $type ; ?> {		
			template <std::size_t N, std::size_t M, typename T>
			struct tmat;					
			
			template <typename... T, std::size_t N, typename R = std::common_type_t<T...>>
			inline auto tmat_make (const tvec<N, T>&... args) {
<?php if ($type == 'row_major'): ?>
				return tmat<N, sizeof... (args), R> (args...);
<?php else: ?>
				return tmat<sizeof... (args), N, R> (args...);
<?php endif; ?>
			}		
/*			
			template <std::size_t _Cols, std::size_t _Rows, typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<_Cols, _Rows, _Lhs>& lhs, const tvec<_Cols, _Rhs>& rhs) {
				tvec<_Rows, _Ret> result;
				for (auto j = 0u; j < result.size; ++j) {
					result [j] = dot (row (lhs, j), rhs);
				}
				return result;
			}

			template <std::size_t _Cols, std::size_t _Rows, typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tvec<_Rows, _Lhs>& lhs, const tmat<_Cols, _Rows, _Rhs>& rhs) {
				tvec<_Cols, _Ret> result;
				for (auto j = 0u; j < result.size; ++j) {
					result [j] = dot (lhs, col (rhs, j));
				}
				return result;
			}			
*/			
			template <typename T>
			auto det (const tmat<2, 2, T>& in) {
				const auto& a = in [0];
				const auto& b = in [1];				
				return a.x*b.y - b.x*a.y;
			}
			
			template <typename T>
			auto det (const tmat<3, 3, T>& in) {
				const auto& a = in [0];
				const auto& b = in [1];
				const auto& c = in [2];
				return dot (a, cross (b, c));
			}
			
			template <typename T>
			auto det (const tmat<4, 4, T>& in) {
				const auto& a = in [0];
				const auto& b = in [1];
				const auto& c = in [2];
				const auto& d = in [3];
				return a.x*(b.y*(c.z*d.w - c.w*d.z) + b.z*(c.w*d.y - c.y*d.w) + b.w*(c.y*d.z - c.z*d.y))
					 + a.y*(b.x*(c.w*d.z - c.z*d.w) + b.z*(c.x*d.w - c.w*d.x) + b.w*(c.z*d.x - c.x*d.z))
					 + a.z*(b.x*(c.y*d.w - c.w*d.y) + b.y*(c.w*d.x - c.x*d.w) + b.w*(c.x*d.y - c.y*d.x))
					 + a.w*(b.x*(c.z*d.y - c.y*d.z) + b.y*(c.x*d.z - c.z*d.x) + b.z*(c.y*d.x - c.x*d.y));
 				/*
				return dot (a, 
					+b.yxxx*(c.zwyz*d.wzwy - c.wzwy*d.zwyz)
					+b.zzyy*(c.wxwx*d.ywxz - c.ywxz*d.wxwx)
					+b.wwwz*(c.yzxy*d.zxyx - c.zxyx*d.yzxy)				
				);
				*/
			}
			
			template <typename T>
			auto adjugate (const tmat<2, 2, T>& in) {
				const auto& a = in [0];
				const auto& b = in [1];
				return tmat<2, 2, T> (+b.y, -a.y, -b.x, +a.x);
			}
			
			template <typename T>
			auto adjugate (const tmat<3, 3, T>& m) {
				const auto& a = m [0];
				const auto& b = m [1];
				const auto& c = m [2];				
				return tmat<3, 3, T> (
					b.y*c.z - b.z*c.y, a.z*c.y - a.y*c.z, a.y*b.z - a.z*b.y,
					b.z*c.x - b.x*c.z, a.x*c.z - a.z*c.x, a.z*b.x - a.x*b.z,
					b.x*c.y - b.y*c.x, a.y*c.x - a.x*c.y, a.x*b.y - a.y*b.x
				);
			}
			
			template <typename T>
			auto adjugate (const tmat<4, 4, T>& m) {
				const auto& a = m [0];
				const auto& b = m [1];
				const auto& c = m [2];
				const auto& d = m [3];
				return tmat<4, 4, T> (
					 b.w*(c.y*d.z - d.y*c.z) + c.w*(d.y*b.z - b.y*d.z) + d.w*(b.y*c.z - c.y*b.z),
				     a.y*(c.w*d.z - c.z*d.w) + c.y*(a.z*d.w - a.w*d.z) + d.y*(a.w*c.z - a.z*c.w),
					 a.y*(b.z*d.w - b.w*d.z) + b.y*(a.w*d.z - a.z*d.w) + d.y*(a.z*b.w - a.w*b.z),
					 a.y*(b.w*c.z - b.z*c.w) + b.y*(a.z*c.w - a.w*c.z) + c.y*(a.w*b.z - a.z*b.w),
					 b.x*(c.w*d.z - c.z*d.w) + c.x*(b.z*d.w - b.w*d.z) + d.x*(b.w*c.z - b.z*c.w),
					 a.x*(c.z*d.w - c.w*d.z) + c.x*(a.w*d.z - a.z*d.w) + d.x*(a.z*c.w - a.w*c.z),
					 a.x*(b.w*d.z - b.z*d.w) + b.x*(a.z*d.w - a.w*d.z) + d.x*(a.w*b.z - a.z*b.w),
					 a.x*(b.z*c.w - b.w*c.z) + b.x*(a.w*c.z - a.z*c.w) + c.x*(a.z*b.w - a.w*b.z),
					 b.x*(c.y*d.w - c.w*d.y) + c.x*(b.w*d.y - b.y*d.w) + d.x*(b.y*c.w - b.w*c.y),
					 a.x*(c.w*d.y - c.y*d.w) + c.x*(a.y*d.w - a.w*d.y) + d.x*(a.w*c.y - a.y*c.w),
					 a.x*(b.y*d.w - b.w*d.y) + b.x*(a.w*d.y - a.y*d.w) + d.x*(a.y*b.w - a.w*b.y),
					 a.x*(b.w*c.y - b.y*c.w) + b.x*(a.y*c.w - a.w*c.y) + c.x*(a.w*b.y - a.y*b.w),
					 b.x*(c.z*d.y - c.y*d.z) + c.x*(b.y*d.z - b.z*d.y) + d.x*(b.z*c.y - b.y*c.z),
					 a.x*(c.y*d.z - c.z*d.y) + c.x*(a.z*d.y - a.y*d.z) + d.x*(a.y*c.z - a.z*c.y),
					 a.x*(b.z*d.y - b.y*d.z) + b.x*(a.y*d.z - a.z*d.y) + d.x*(a.z*b.y - a.y*b.z),
					 a.x*(b.y*c.z - b.z*c.y) + b.x*(a.z*c.y - a.y*c.z) + c.x*(a.y*b.z - a.z*b.y)
				);			
			}
			
			template <typename T, std::size_t N, std::size_t M, std::enable_if_t<std::is_integral<T>::value, int> = 0>
			auto inverse (const tmat<N, M, T>& in) {
				const auto t = det (in);
				return t == 0 ? tmat<N, M, T> () : adjugate (in) / t;
			}
			
			template <typename T, std::size_t N, std::size_t M, std::enable_if_t<std::is_floating_point<T>::value, int> = 0>
			auto inverse (const tmat<N, M, T>& in) {
				const auto t = det (in);
				return t == 0 ? tmat<N, M, T> () : adjugate (in) * (T(1)/t);
			}
			
		}
<?php endforeach; ?>
	}
}
#include "utils.hpp"
namespace std {
<?php foreach(['col_major', 'row_major'] as $type): ?>
	template <std::size_t N, std::size_t M, typename T> 
	struct hash<<?php echo $G_namespace ; ?>::<?php echo $G_module_name ;?>::<?php echo $type ; ?>::tmat<N, M, T>> {
		std::size_t operator () (const <?php echo $G_namespace ; ?>::<?php echo $G_module_name ;?>::<?php echo $type ; ?>::tmat<N, M, T>& x) const  {
			return <?php echo $G_namespace ; ?>::detail::fnv_array (reinterpret_cast<const std::uint8_t (&) [sizeof (x)]> (x));
		}
	};
<?php endforeach; ?>
}
