<?php 
	require_once 'tmp_tvec_functions.php';

	global $G_member_naming, $G_vector_max_size ; 
		
	$primary_naming = $G_member_naming [0];
			
	$primary_args = [];
	$primary_xtra = [];
	$primary_init = [];
	
	for ($i = 0; $i < $G_current_vector_size; ++$i) {
		$argi_name = sprintf ('v%d', $i);		
		
		$primary_args [] = sprintf ('T %s', $argi_name);
		if ($i < count ($primary_naming)) {
			$init_name = $primary_naming [$i];
			$primary_init [] = sprintf ('%s (%s)', $init_name, $argi_name);
			continue;
		}
		$primary_xtra [] = $argi_name;
	}
	
	$S_primary_args = implode (', ', $primary_args);
	$S_primary_init = implode (', ', $primary_init);
	$S_primary_xtra = implode (', ', $primary_xtra);
	
	$bin_ops = [
		'+' => '%s + %s', 
		'-' => '%s - %s', 
		'*' => '%s * %s',
		'/' => '%s / %s',
		'%' => 'modulus (%s, %s)',
		'&' => '%s & %s',
		'|' => '%s | %s',
		'^' => '%s ^ %s',
	   '&&' => '%s && %s',
	   '||' => '%s || %s',
	];
	
	$uni_ops = [
		'+' => '+%s',
		'-' => '-%s',
		'~' => '~%s',
		'!' => '!%s'
	];
	
	$bool_ops = [
		'==' => ['%s == %s', '&&'],
		'!=' => ['%s != %s', '||']
	];	
	
	$std_math = [
		'abs' 	=> ['x'],
		'min' 	=> ['x', 'y'],	
		'max' 	=> ['x', 'y'],
			
		'sin' 	=> ['x'],
		'cos' 	=> ['x'],
		'tan' 	=> ['x'],
		'sinh' 	=> ['x'],
		'cosh' 	=> ['x'],
		'tanh' 	=> ['x'],
		'asin' 	=> ['x'],
		'acos' 	=> ['x'],
		'atan' 	=> ['x'],
		'atan2'	=> ['x', 'y'],
		
		'log'   => ['x'],
		'log2'  => ['x'],
		'log10' => ['x'],
		'log1p' => ['x'],
		
		'exp'   => ['x'],
		'exp2'  => ['x'],
		'exp1m' => ['x'],		
		'pow'   => ['x', 'n'],
		'sqrt'  => ['x'],
		'cbrt'  => ['x'],
		
		'floor' => ['x'],
		'ceil'  => ['x'],
		'round' => ['x'],
		'trunc' => ['x'],
		'copysign' => ['x', 'y']
	];
?>

#include "basic_tvec.hpp"

#include <istream>
#include <ostream>
#include <cmath>
#include <type_traits>

#pragma pack(push, 1)
namespace <?php global $G_namespace ; echo $G_namespace ; ?> {
	namespace <?php global $G_module_name ;  echo $G_module_name ; ?> {
		namespace detail {
			template<typename T>
			struct tvec_landing<<?php echo $G_current_vector_size; ?>, T> {				
				tvec_landing (<?php 
					$ass = $set = $out = [];
					for ($i = 0; $i < $G_current_vector_size; ++$i) {
						$out [] = sprintf ('T& %s', make_tvec_name ($i));
					    $set [] = sprintf ('%s (%s)', make_tvec_name ($i), make_tvec_name ($i));
						$ass [] = sprintf ('%s = %s', make_tvec_name ($i), get_tvec_value ($i, 'in'));
					}
					echo implode (', ', $out);
				?>) : <?php
					echo implode (', ', $set);
				?> {}
				
				tvec_landing<<?php echo $G_current_vector_size; ?>, T>& operator = (const tvec<<?php echo $G_current_vector_size ; ?>, T>& in)  {
<?php foreach ($ass as $line): ?>
					<?php echo $line ; ?>;
<?php endforeach; ?>
					return *this;
				}
				
			private:
<?php foreach ($out as $line): ?>
				<?php echo $line ?>;
<?php endforeach; ?>
			};
		}
				
		template <typename T>
		struct tvec<<?php echo $G_current_vector_size ; ?>, T> {
			typedef T value_type;
			static const std::size_t size = <?php echo $G_current_vector_size ?>;			

			union {				
<?php $max_len = 0; ?>
<?php foreach ($G_member_naming as $alt_naming): ?>
<?php $max_len = max($max_len, count ($alt_naming)); ?>
				struct { value_type <?php echo implode (', ', array_slice ($alt_naming, 0, $G_current_vector_size)) ?>; };
<?php endforeach; ?>
<?php global $G_vector_sizes ; ?>

<?php 
	$swizzle_list = [];
	foreach ($G_member_naming as $naming) {
		$current_list = build_swizzle_permutations ($G_vector_sizes, 
			array_slice (array_slice ($naming, 0, $G_current_vector_size), 0, $G_vector_max_size));		
		$swizzle_list = array_merge ($swizzle_list, $current_list); 
	}
?>
			#if !<?php echo make_macro_name ('DISABLE_SWIZZLE'); ?>
			
<?php foreach ($swizzle_list as $swizzle): ?>
				detail::tvec_swizzle<T, <?php echo implode (', ', $swizzle [1]) ; ?>> <?php echo $swizzle [0] ; ?>;
<?php endforeach; ?>
			#endif
			};
<?php if ($G_current_vector_size > $max_len): ?>
		private:
			T m_extra [<?php echo $max_len; ?>];
		public:
<?php endif; ?>
<?php foreach([['const', 'T'], ['', 'T']] as $type): ?>
			<?php echo implode (' ', $type) ; ?>& operator [] (std::size_t i)<?php echo $type[0]; ?> {
			#if <?php echo make_macro_name ('RANGE_CHECK'); ?>
			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast<<?php echo implode (' ', $type) ; ?> (&) [size]> (*this) [i];
			}
<?php endforeach; ?>
			
			constexpr tvec (<?php echo $S_primary_args ; ?>) : 
				<?php echo $S_primary_init ; ?>
				<?php if (strlen ($S_primary_xtra) > 0): ?>, m_extra {<?php echo $S_primary_xtra ; ?>} <?php endif; ?> 
			{}
			
			constexpr tvec (T in) : 
				tvec (in<?php echo str_repeat (', in', $G_current_vector_size - 1) ; ?>) 
			{}
			
			constexpr tvec ()  : tvec (T ()) {}
			
			//template <typename U, std::size_t... I>
			//tvec (const detail::tvec_swizzle<U, I...>& a) : tvec (a ()) {}
			
<?php for ($i = 2; $i <= $G_vector_max_size; ++$i): ?>
			template <typename I<?php if ($G_current_vector_size == $i) { ?>, std::enable_if_t<!std::is_same<T, I>::value, int> = 0<?php } ?>> 
			explicit tvec (const tvec<<?php echo $i ; ?>, I>& in) : 
				tvec (<?php unwrap_argument_list ($i, 'T ()', $G_current_vector_size, $G_vector_max_size) ; ?>) 
			{}
			
<?php endfor; ?>
<?php foreach (build_tvec_initializers($G_current_vector_size) as $line): ?>
			<?php echo $line; ?>
<?php endforeach; ?>
			tvec<<?php echo $G_current_vector_size ; ?>, T>& operator = (const tvec<<?php echo $G_current_vector_size ; ?>, T>& in) {
<?php for ($i = 0; $i < $G_current_vector_size; ++$i): ?>
				<?php echo get_tvec_value ($i, '(*this)'); ?> = <?php echo get_tvec_value ($i, 'in') ; ?>;
<?php endfor; ?>
				return *this;
			};
		};
////////////////////////////////
// I/O Operators
////////////////////////////////
		template <typename T>
		std::istream& operator >> (std::istream& iss, tvec<<?php echo $G_current_vector_size ; ?>, T>& in) {			
			return iss<?php 
				for ($i = 0; $i < $G_current_vector_size; ++$i) {
					printf (' >> %s', get_tvec_value ($i, 'in'));
				}
			?>;
		}		
		template <typename T>
		std::ostream& operator << (std::ostream& oss, const tvec<<?php echo $G_current_vector_size ; ?>, T>& in) {
			return oss << "{ " <?php 
				for ($i = 0; $i < $G_current_vector_size; ++$i) {
					printf ('<< "%s: " << %s << ", "', make_tvec_name ($i, '[', ']'), get_tvec_value ($i, 'in'));
				}
			?> << " }";
		}

////////////////////////////////
// Binary operations
////////////////////////////////
<?php foreach ($bin_ops as $operator => $operation): ?>
		template <typename A, typename B>
		inline auto operator <?php echo $operator ?> (const tvec<<?php echo $G_current_vector_size ; ?>, A>& a, const tvec<<?php echo $G_current_vector_size ; ?>, B>& b) {
			return tvec_make (<?php
				$unwrapped = []; 
				for ($i = 0; $i < $G_current_vector_size; ++$i) { 					
					$a = get_tvec_value ($i, 'a'); 
					$b = get_tvec_value ($i, 'b');					
					$unwrapped [] = sprintf ($operation, $a, $b);
				} 
				echo implode (', ', $unwrapped);
			?>);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator <?php echo $operator ?> (const tvec<<?php echo $G_current_vector_size ; ?>, A>& a, const B& b) {
			return a <?php echo $operator ?> tvec<<?php echo $G_current_vector_size ; ?>, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator <?php echo $operator ?> (const A& a, const tvec<<?php echo $G_current_vector_size ; ?>, B>& b) {
			return tvec<<?php echo $G_current_vector_size ; ?>, A> (a) <?php echo $operator ?> b;
		}
		
<?php endforeach; ?>
////////////////////////////////
// Boolean operations
////////////////////////////////
<?php foreach ($bool_ops as $operator => $operation): ?>
		template <typename A, typename B>
		inline auto operator <?php echo $operator ?> (const tvec<<?php echo $G_current_vector_size ; ?>, A>& a, const tvec<<?php echo $G_current_vector_size ; ?>, B>& b) {
			return <?php
				$unwrapped = []; 
				for ($i = 0; $i < $G_current_vector_size; ++$i) { 					
					$a = get_tvec_value ($i, 'a');
					$b = get_tvec_value ($i, 'b');
					$unwrapped [] = sprintf ('('.$operation [0].')', $a, $b);
				} 
				echo implode (sprintf (' %s ', $operation [1]), $unwrapped);
			?>;			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator <?php echo $operator ?> (const tvec<<?php echo $G_current_vector_size ; ?>, A>& a, const B& b) {
			return a <?php echo $operator ?> tvec<<?php echo $G_current_vector_size ; ?>, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator <?php echo $operator ?> (const A& a, const tvec<<?php echo $G_current_vector_size ; ?>, B>& b) {
			return tvec<<?php echo $G_current_vector_size ; ?>, A> (a) <?php echo $operator ?> b;
		}		
		
<?php endforeach; ?>		


////////////////////////////////
// Unary operations
////////////////////////////////
<?php foreach ($uni_ops as $operator => $operation): ?>
		template <typename T>
		inline auto operator <?php echo $operator ?> (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a) {
			return tvec_make (<?php
				$unwrapped = []; 
				for ($i = 0; $i < $G_current_vector_size; ++$i) { 
					$a = get_tvec_value ($i, 'a');
					$unwrapped [] = sprintf ($operation, $a);
				} 
				echo implode (', ', $unwrapped);
			?>);			
		}
		
<?php endforeach; ?>
////////////////////////////////
// Misc operations
////////////////////////////////
		template <typename A, typename B>
		inline auto dot (const tvec<<?php echo $G_current_vector_size ; ?>, A>& a, const tvec<<?php echo $G_current_vector_size ; ?>, B>& b)  {			
			return <?php 
				$out = [];
				for ($i = 0; $i < $G_current_vector_size; ++$i) {
					$out [] = sprintf ('%s*%s', 
						get_tvec_value ($i, 'a'),
						get_tvec_value ($i, 'b')
					);
				}
				echo implode (' + ', $out);
			?>;
		}
		
		template <typename T>
		inline auto hypot2 (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a)  {
			return dot (a, a);
		}
		
		template <typename T>
		inline auto hypot (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a)  {
			return std::sqrt (hypot2 (a));
		}

		template <typename T>
		inline auto length2 (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a)  {
			return hypot2 (a);
		}
		
		template <typename T>
		inline auto length (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a)  {
			return hypot (a);
		}
		
		template <typename T>
		inline auto normalize (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a)  {
			return a / hypot (a);
		}	
		
		template <typename T>
		inline auto distance (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a, const tvec<<?php echo $G_current_vector_size ; ?>, T>& b)  {
			return hypot (b - a);
		}

		template <typename T>
		inline auto distance2 (const tvec<<?php echo $G_current_vector_size ; ?>, T>& a, const tvec<<?php echo $G_current_vector_size ; ?>, T>& b)  {
			return hypot2 (b - a);
		}
		
<?php if ($G_current_vector_size == 3): ?>
		template <typename T, std::size_t N>
		inline auto cross (const tvec<N, T>& a, const tvec<N, T>& b)  {			
			static_assert (N == 3, "Crossproduct only defined for 3 dimentional vectors.");
			return a.yzx * b.zxy - a.zxy * b.yzx;
		}		
<?php endif; ?>

<?php foreach ($std_math as $fun => $args): ?>
		template <typename T>
		auto <?php echo $fun ; ?> (<?php
			$out = [];
			foreach ($args as $name) {
				$out [] = sprintf ('const tvec<%d, T>& %s', $G_current_vector_size, $name);
			}
			echo implode (', ', $out);
		?>)  {
			return tvec_make (<?php 
				$out = [] ;				
				for ($i = 0; $i < $G_current_vector_size; ++$i){
					$arg = [] ;
					foreach ($args as $name) {
						$arg [] = get_tvec_value ($i, $name);
					}
					$out [] = sprintf ('std::%s (%s)', $fun, implode (', ', $arg));
				}
				echo implode (', ', $out);
			?>);
		}		
		
<?php endforeach; ?>
		
	}
}
#pragma pack(pop)
