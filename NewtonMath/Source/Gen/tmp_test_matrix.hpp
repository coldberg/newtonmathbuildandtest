#include <Math/matrix.hpp>

<?php
$test_matrixes = [  
	['data' => [[1,  2], [ 3, 4]], 'det' => -2, 'inv' => [], 'vec' => [1, 2]],
	['data' => [[5,  4], [ 1, 1]], 'det' =>  1, 'inv' => [], 'vec' => [1, 2]],
	['data' => [[5,  3], [-1, 4]], 'det' => 23, 'inv' => [], 'vec' => [1, 2]],
	['data' => [[1, -4], [ 0, 3]], 'det' =>  3, 'inv' => [], 'vec' => [1, 2]],
	['data' => [[2,  1], [-1, 3]], 'det' =>  7, 'inv' => [], 'vec' => [1, 2]],
		
	['data' => [[ 1,  2,  3], [ 0, -4,  1], [ 0,  3, -1]], 'det' =>   1, 'inv' => [], 'vec' => [1, 2, 3]],
	['data' => [[ 5, -2,  1], [ 0,  3, -1], [ 2,  0,  7]], 'det' => 103, 'inv' => [], 'vec' => [1, 2, 3]],
	['data' => [[ 2, -3, -2], [-6,  3,  3], [-2, -3, -2]], 'det' =>  12, 'inv' => [], 'vec' => [1, 2, 3]],
	['data' => [[-4,  5,  2], [-3,  4,  2], [-1,  2,  5]], 'det' =>  -3, 'inv' => [], 'vec' => [1, 2, 3]],
		
	['data' => [[1, 4,  2,  3], [0, 1, 4,  4], [-1, 0, 1,  0], [ 2, 0, 4, 1]], 'det' =>    65, 'inv' => [], 'vec' => [1, 2, 3, 4]],
	['data' => [[3, 2, -1,  4], [2, 1, 5,  7], [ 0, 5, 2, -6], [-1, 2, 1, 0]], 'det' =>  -418, 'inv' => [], 'vec' => [1, 2, 3, 4]],
	['data' => [[3, 0,  2, -1], [1, 2, 0, -2], [ 4, 0, 6, -3], [ 5, 0, 2, 0]], 'det' =>    20, 'inv' => [], 'vec' => [1, 2, 3, 4]],
	
	
];

function transpose ($mat) {
	$out = [];
	for ($j = 0; $j < 4; ++$j) {		
		$outl = [];
		for ($i = 0; $i < count ($mat); ++$i) {
			if (count ($mat [$i]) < 1)
				continue;
			$outl [] = array_shift ($mat [$i]);
		}
		if (count ($outl)) {
			$out [] = $outl;
			continue;
		}
		break;
	}	
	return $out;
}

function the_matrix ($mat) {	
	$out = [];			
	foreach ($mat as $line) {
		$out [] = sprintf ('tvec_make (%s)', implode (', ', $line));
	}
	echo implode (', ', $out);
}
?>

<?php foreach (['col', 'row'] as $type): ?>
void test_matrix_<?php echo $type ; ?>_major () {
	using namespace <?php echo implode ('::', [$G_namespace, $G_module_name]); ?>;
	using namespace <?php echo implode ('::', [$G_namespace, $G_module_name, $type.'_major']); ?>;
	// Test determinant 	
<?php 
	foreach ($test_matrixes as $test) {		
?>
	([] () {
		auto m = tmat_make (<?php  $type != 'col' ? the_matrix ($test['data']): the_matrix (transpose ($test ['data'])) ; ?>);
		auto n = transpose (m);
	
		std::cout << m << "\n";
		std::cout << n << "\n";
		std::cout << "det = " << det (m) << "\n";
		
		assert (det (m) == <?php echo $test ['det']; ?>);
		assert (det (n) == <?php echo $test ['det']; ?>);
	
		auto om = adjugate (m) * m;
		auto on = adjugate (n) * n;
		
		std::cout << om << "\n";
		std::cout << on << "\n";
		
		assert (om == decltype (om) (1) * det (m));
		assert (on == decltype (on) (1) * det (n));
		
		assert (om*tvec_make (<?php echo implode (', ', $test ['vec']); ?>) == tvec_make (<?php echo implode (', ', $test ['vec']); ?>)*det (m));
		assert (on*tvec_make (<?php echo implode (', ', $test ['vec']); ?>) == tvec_make (<?php echo implode (', ', $test ['vec']); ?>)*det (n));
		
		std::cout << "---^\n";

	}) ();
<?php		
	}
?>
	
}
<?php endforeach; ?>