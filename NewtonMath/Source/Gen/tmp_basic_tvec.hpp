<?php global $G_member_naming ; ?>

#include <cmath>
#include <algorithm>

#pragma pack(push, 1)
namespace <?php global $G_namespace ; echo $G_namespace ; ?> {
	namespace <?php global $G_module_name ;  echo $G_module_name ; ?> {
		
		template <std::size_t N, typename T>
		struct tvec;
				
		template <typename...Args>
		auto tvec_make (Args&&...args) {
			typedef std::common_type_t<std::remove_reference_t<Args>...> value_type;
			return tvec<sizeof... (Args), value_type> (std::forward<Args>(args)...);
		}		
		
		namespace detail {
						
			template <typename T, T I0, T... I>
			struct static_max {
				static const T value = static_max<T, I0, static_max<T, I...>::value>::value;
			};
			
			template <typename T, T I0, T I1>
			struct static_max<T, I0, I1> {
				static const T value = I0 > I1 ? I0 : I1;
			};
			
			template <std::size_t N, typename T>
			struct tvec_landing;
			
			template <typename T, std::size_t... I>
			struct tvec_swizzle {				
				typedef tvec<sizeof... (I), T> value_type;								
				static const auto last_component = static_max<std::size_t, I...>::value;
				static const auto size = sizeof... (I);
				
				value_type operator () () const  {				
					return tvec_make (_vec [I]...);
				}
				
				operator value_type () const  {
					return (*this) ();
				};
				
                tvec_swizzle& operator = (const value_type& in) {                    
					tvec_landing<sizeof... (I), T> (_vec [I]...) = in;
                    return *this;
                };				
				
				tvec_swizzle& operator = (const tvec_swizzle<T, I...>& in)  {
					return *this = in();
				}
				
				template<std::size_t... J>
				tvec_swizzle& operator = (const tvec_swizzle<T, J...>& in)  {
					return *this = in();
				}
				
				tvec_swizzle (const tvec_swizzle<T, I...>&) = delete;
				tvec_swizzle (tvec_swizzle<T, I...>&&) = delete;
			private:
				T _vec [last_component + 1u];
			};
			
			template<typename T, std::size_t... I>
			std::ostream& operator << (std::ostream& oss, const tvec_swizzle<T, I...>& ov) {
				return oss << ov ();
			}
			
			template<typename T, std::size_t... I>
			std::istream& operator >> (std::istream& iss, tvec_swizzle<T, I...>& iv) {
				tvec<sizeof...(I), T> _iv;
				iss >> _iv;
				iv = _iv;
				return iss;
			}			
			
<?php foreach (['*', '/', '+', '-', '%', '&', '|', '^', '&&', '||', '==', '!='] as $op): ?>
			template <typename A, std::size_t... I, typename B, std::size_t... J>
			inline auto operator <?php echo $op ;?> (const tvec_swizzle<A, I...>& a, const tvec_swizzle<B, J...>& b)  {
				return a () * b ();
			}
			
			template <typename A, std::size_t... I, typename B>
			inline auto operator <?php echo $op ;?> (const tvec_swizzle<A, I...>& a, const B& b)  {
				return a () * b;
			}
			
			template <typename A, typename B, std::size_t... J>
			inline auto operator <?php echo $op ;?> (const A& a, const tvec_swizzle<B, J...>& b)  {
				return a * b ();
			}
			
<?php endforeach; ?>

<?php foreach (['+', '-', '~', '!'] as $op): ?>
			template <typename A, std::size_t... I>
			inline auto operator <?php echo $op ;?> (const tvec_swizzle<A, I...>& a)  {
				return <?php echo $op; ?>a () ;
			}
			
<?php endforeach; ?>
		}		
				
		template <typename A, typename B>
		inline auto modulus (A a, B b) { return a % b; }
		inline auto modulus ( float a,  float b) { return std::fmod (a, b); }
		inline auto modulus (double a, double b) { return std::fmod (a, b); }
		inline auto modulus ( float a, double b) { return std::fmod ((double)a, b); }
		inline auto modulus (double a,  float b) { return std::fmod (a, (double)b); }				
	}	
}

#include "utils.hpp"
namespace std {	
	template <std::size_t N, typename T> 
	struct hash<<?php echo $G_namespace ; ?>::<?php echo $G_module_name ;?>::tvec<N, T>> {
		std::size_t operator () (const <?php echo $G_namespace ; ?>::<?php echo $G_module_name ;?>::tvec<N, T>& x) const  {
			return <?php echo $G_namespace ; ?>::detail::fnv_array (reinterpret_cast<const std::uint8_t (&) [sizeof (x)]> (x));
		}
	};
}
#pragma pack(pop)
