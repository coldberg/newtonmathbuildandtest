namespace <?php echo $G_namespace ; ?> {
	namespace utils {
		template <typename T, std::size_t N>
		inline std::size_t fnv_array (const T (&_data) [N])  {
			static const auto _is32bt = sizeof (std::size_t) == sizeof (std::uint32_t);
			static const auto _fprime = _is32bt ? 16777619ul   : 1099511628211ull ;
			static const auto _offset = _is32bt ? 2166136261ul : 14695981039346656037ull ;		
			auto _hash = (std::size_t)_offset;
			std::for_each (
				_data.begin (), 
				_data.end (), 
				[&_hash] (auto _octet) {
					_hash ^= _octet;
					_hash *= _prime;
				}
			);			
		}
	}
}

