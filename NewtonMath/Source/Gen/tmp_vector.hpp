<?php foreach ($G_vector_sizes as $fid): ?>
#include "detail/tvec<?php echo $fid ; ?>.hpp"
<?php endforeach; ?>

namespace <?php echo $G_namespace ; ?> {
	namespace <?php echo $G_module_name ; ?> {
	
<?php global $G_types ; ?>	
<?php foreach ($G_vector_sizes as $fid): ?>
		template <typename T> using tvec<?php echo $fid ?> = tvec<<?php echo $fid ; ?>, T>;
<?php endforeach; ?>
<?php foreach ($G_types as $type): ?>
		template <std::size_t N> using <?php echo $type ['short'] ; ?>vec = tvec<N, <?php echo $type ['long'] ; ?>>;
<?php endforeach; ?>

<?php foreach ($G_vector_sizes as $fid): ?>
<?php foreach ($G_types as $type): ?>
		typedef tvec<<?php echo $fid; ?>, <?php echo $type ['long'] ;?>> <?php echo $type ['short'] ; ?>vec<?php echo $fid ; ?>;
<?php endforeach; ?>
<?php endforeach; ?>

	}
}

