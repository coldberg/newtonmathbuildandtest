 //matrix
 
<?php foreach ($G_vector_sizes as $Nsize):?>
<?php foreach ($G_vector_sizes as $Msize):?>
 #include "detail/tmat_<?php echo $Nsize ?>x<?php echo $Msize ; ?>_row_major.hpp"
 #include "detail/tmat_<?php echo $Nsize ?>x<?php echo $Msize ; ?>_col_major.hpp"
<?php endforeach; ?>
<?php endforeach; ?>


namespace <?php echo $G_namespace ; ?> {
	namespace <?php echo $G_module_name ; ?> {
<?php foreach (['col', 'row'] as $type): ?>
		namespace <?php echo $type ; ?>_major {
<?php foreach ($G_vector_sizes as $Rsize): ?>
<?php foreach ($G_vector_sizes as $Csize): ?>
			template <typename T> using tmat<?php echo implode ('x', [$Csize, $Rsize]) ; ?> = tmat<<?php echo implode (', ', [$Csize, $Rsize]) ; ?>, T>;			
<?php foreach ($G_types as $type): ?>
			typedef tmat<?php echo implode('x', [$Csize, $Rsize]) ; ?><<?php echo $type['long']; ?>> <?php echo $type['short'] ; ?>mat<?php echo implode('x', [$Csize, $Rsize]) ; ?>;
<?php endforeach; ?>
<?php endforeach; ?>
			template <typename T> using tmat<?php echo $Rsize ; ?> = tmat<<?php echo implode (', ', [$Rsize, $Rsize]) ; ?>, T>;
<?php foreach ($G_types as $type): ?>
			typedef tmat<?php echo $Rsize ; ?><<?php echo $type['long']; ?>> <?php echo $type['short'] ; ?>mat<?php echo $Rsize ; ?>;
<?php endforeach; ?>
<?php endforeach; ?>
<?php foreach ($G_types as $type): ?>
			template <std::size_t _Cols, std::size_t _Rows> using <?php echo $type['short'] ; ?>mat = tmat<_Cols, _Rows, <?php echo $type['long'] ; ?>> ;
<?php endforeach; ?>
		}
<?php endforeach; ?>
		
	}
}

 