<?php
	$G_types = [
		['short' => 'u8' , 	'long' => 'std::uint8_t', 		'suffix'=>nil	],
		['short' => 'u16', 	'long' => 'std::uint16_t',		'suffix'=>nil	],
		['short' => 'u32', 	'long' => 'std::uint32_t',		'suffix'=>'u'	],
		['short' => 'u64', 	'long' => 'std::uint64_t',		'suffix'=>'ull'	],		
		['short' => 'i8' , 	'long' => 'std::int8_t', 		'suffix'=>nil	],
		['short' => 'i16', 	'long' => 'std::int16_t',		'suffix'=>nil	],
		['short' => 'i32', 	'long' => 'std::int32_t',		'suffix'=>nil	],
		['short' => 'i64', 	'long' => 'std::int64_t',		'suffix'=>'ll'	],
		['short' => 'f32', 	'long' => 'float',		 		'suffix'=>'f'	],
		['short' => 'f64', 	'long' => 'double',		 		'suffix'=>nil	],		
		['short' => 'll', 	'long' => 'long long',	 		'suffix'=>'ll'	],
		['short' => 'l', 	'long' => 'long',		 		'suffix'=>'l'	],		
		['short' => 'llu', 	'long' => 'unsigned long long',	'suffix'=>'ull'	],
		['short' => 'u', 	'long' => 'unsigned',	 		'suffix'=>'u'	],				
		['short' => 'i', 	'long' => 'int',				'suffix'=>nil	],
		['short' => 'b', 	'long' => 'bool',				'suffix'=>nil	],
		['short' => 'd', 	'long' => 'double',				'suffix'=>nil	],	
		['short' => 'f', 	'long' => 'float',				'suffix'=>'f'	],
		['short' => '', 	'long' => 'float',				'suffix'=>'f'	]
		
	];
	
	$G_member_naming = [
		['x', 'y', 'z', 'w'],  
		['r', 'g', 'b', 'a'],
		['s', 't', 'p', 'q'],
		['u', 'v', 'i', 'j']
	];	
	
	$G_vector_sizes = [2, 3, 4];
	$G_vector_max_size = max ($G_vector_sizes);
	
	$G_namespace = 'Newton';
	$G_module_name = 'Math'; 
	$G_indent_size = 4;
	$G_indent_level = 0;
	
	function u_remove_recursive ($dirname) {
		foreach (glob ($dirname . "/*") as $file) {
			if (is_dir ($file)) {
				if (!u_remove_recursive ($file))
					return false;
				continue;
			}
			if (!unlink ($file)) {				
				return false;
			}
		}		
		return rmdir ($dirname);
	}

	function u_delete_file ($fname) {
		if (!file_exists ($fname)){
			return true;
		}
		if (is_dir ($fname)) {
			return u_remove_recursive ($fname);
		}			
		return unlink ($fname);
	}

	function safe_call ($fname, $args) {		
		printf ("%s %s ... ", $fname, count ($args) ? "(`" . implode ('`,`', $args) . "`)" : "");
		if (!call_user_func_array ($fname, $args)) {
			printf ("error!\n");
			exit (-1);
		}
		printf ("success\n");
	}
	
	function prepend_indent ($in) {
		global $G_indent_size;
		if (strlen (trim ($in)) > 0) {
			return str_repeat (' ', $G_indent_size) . $in;		
		}
		return $in;
	}
		
	function indent_enter () {		
		ob_start ();
	}
	
	function indent_leave () {		
		$whole = ob_get_clean ();
		$lines = explode ("\n", preg_replace ('/\R/', "\n", $whole));
		$lines = array_map ('prepend_indent', $lines);		
		$whole = implode ("\n", $lines);
		echo $whole;
	}	
	
	function make_include_guard ($filename) {
		global $G_namespace, $G_module_name;
		$la = function ($sub) { return strtoupper (preg_replace ('/[^a-zA-Z0-9]+/', '_', $sub)); };
		$ig = array_map ($la, [$G_namespace, $G_module_name, $filename]);
		return '__'.implode ('_', $ig).'__';
	}
	
	function write_template ($template, $data) {
		extract ($data);
		include (dirname (__FILE__) . '/' . $template . '.hpp');
	}
	
	function filter ($in, $out, $data) {
		global $G_namespace, $G_module_name, $G_vector_sizes, $G_types, $G_member_naming;
		$args = compact ('G_namespace', 'G_module_name', 'G_vector_sizes', 'G_types', 'G_member_naming');
		if (is_array ($data)) {
			$args = array_merge ($args, $data);
		}
		ob_start ();
		$include_guard = make_include_guard ($out);
		write_template ('tmp_header', compact ('include_guard', 'G_namespace'));		
		write_template ($in, $args);
		write_template ('tmp_footer', compact ('include_guard', 'G_namespace'));
		file_put_contents ($out, ob_get_clean ());
		return true;
	}	
	
	function autogen_main ($argv) {
		global $G_vector_sizes;
		
		
		safe_call ('chdir', [$argv [1]]);	
				
		safe_call ('u_delete_file', ['detail']);				
		safe_call ('u_delete_file', ['vector.hpp']);		
		safe_call ('u_delete_file', ['matrix.hpp']);
		
		safe_call ('mkdir', ['detail', 0777, true]);		
		
		foreach ($G_vector_sizes as $vsize) {
			safe_call ('filter', ['tmp_tvecn', 'detail/tvec'.$vsize.'.hpp', ['G_current_vector_size'=>$vsize]]);
		}		
		safe_call ('filter', ['tmp_basic_tvec', 'detail/basic_tvec.hpp', nil]);
		safe_call ('filter', ['tmp_vector', 'vector.hpp', nil]);

		foreach ($G_vector_sizes as $Nsize) {
			foreach ($G_vector_sizes as $Msize) {
				safe_call ('filter', ['tmp_tmatnm', sprintf ('detail/tmat_%dx%d_row_major.hpp', $Nsize, $Msize), ['G_matrix_size' => [$Nsize, $Msize], 'G_row_major' => true]]);
				safe_call ('filter', ['tmp_tmatnm', sprintf ('detail/tmat_%dx%d_col_major.hpp', $Nsize, $Msize), ['G_matrix_size' => [$Nsize, $Msize], 'G_row_major' => false]]);
			}
		}		
		safe_call ('filter', ['tmp_basic_tmat', 'detail/basic_tmat.hpp', nil]);
		safe_call ('filter', ['tmp_matrix', 'matrix.hpp', nil]);
		safe_call ('filter', ['tmp_utils', 'detail/utils.hpp', nil]);
		safe_call ('filter', ['tmp_test_matrix', '../test/matrix.hpp', nil]);
		
	}
	
	autogen_main ($argv);